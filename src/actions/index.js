export const LOAD_DECK = 'LOAD_DECK';
export const DECK_LOADED = 'DECK_LOADED';
export const USER_LOAD_CARD = 'USER_LOAD_CARD';
export const DEALER_LOAD_CARD = 'DEALER_LOAD_CARD';
export const USER_CARD_LOADED = 'USER_CARD_LOADED';
export const DEALER_CARD_LOADED = 'DEALER_CARD_LOADED';

export const action = (type, payload) => {
  return {
    type,
    payload,
  }
};

export const setupGame = payload => action(LOAD_DECK, payload);
export const deckLoaded = payload => action(DECK_LOADED, payload);
export const userLoadCard = payload => action(USER_LOAD_CARD, payload);
export const dealerLoadCard = payload => action(DEALER_LOAD_CARD, payload);
export const userCardLoaded = payload => action(USER_CARD_LOADED, payload);
export const dealerCardLoaded = payload => action(DEALER_CARD_LOADED, payload);
