import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';

import reducer from './reducers';

import sagas from './sagas';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const middleware = createSagaMiddleware();

export const store = createStore(
  reducer,
  composeEnhancers(
    applyMiddleware(middleware),
  ),
);

middleware.run(sagas);
