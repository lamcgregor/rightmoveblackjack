import * as actions from '../actions';

const initialState = {
  loading: false,
  deckId: null,
  userCards: [],
  dealerCards: [],
}

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case actions.DEALER_LOAD_CARD :
    case actions.USER_LOAD_CARD :
      return Object.assign({}, state, {
        loading: true,
      });
    case actions.DECK_LOADED :
     return Object.assign({}, state, {
       deckId: payload,
       loading: false,
     });
    case actions.DEALER_CARD_LOADED :
     return Object.assign({}, state, {
       dealerCards: state.dealerCards.concat(payload),
       loading: false,
     });
    case actions.USER_CARD_LOADED :
     return Object.assign({}, state, {
       userCards: state.userCards.concat(payload),
       loading: false,
     });
     default:
      return state;
  }
}

export default reducer;
