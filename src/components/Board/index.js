import React from 'react';

import CardContainer from '../CardContainer';
import styles from './styles.scss';

export default ({
  onDraw,
  onPass,
  userCards,
  dealerCards,
  userTotal,
  dealerTotal,
  userBust,
  dealersTurn,
  winner,
}) => (
  <div className={`${styles.app}`}>
    <div className={`${styles.dealer}`}>
      <div><strong>Dealer's total: {dealerTotal}</strong></div>
      <CardContainer cards={dealerCards} />
    </div>
    {winner &&
      <div className={`${styles.winner}`}>
        {winner} wins!
        <button onClick={() => window.location.reload()}>Restart</button>
      </div>
    }
    <div className={`${styles.user}`}>
      <strong>Your total: {userTotal}</strong>
      <CardContainer cards={userCards} />
      <div>
        <button onClick={() => onDraw()} disabled={userBust || dealersTurn}>Hit</button>
        <button onClick={() => onPass()} disabled={userBust || dealersTurn}>Stick</button>
      </div>
    </div>
  </div>
)
