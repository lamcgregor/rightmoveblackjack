This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

After this the following packages have been added:

- sass-loader
- node-sass

For CSS modules with SASS

- redux
- react-redux
- redux-saga

To showcase redux state-based structure, with sagas as a middleware for async requests

To start up the project, run `yarn install` then when that is complete `yarn start`. Navigate to http://localhost:3000
