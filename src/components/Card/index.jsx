import React, { Component } from 'react';
import styles from './styles.scss';

class Card extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
    }
  }
  componentWillMount() {
    setTimeout(() => this.setState(state => ({loading: false,})),500);
  }
  render() {
    const {
      value,
      image,
      suit,
      code,
    } = this.props;
    const {
      loading
    } = this.state;
    console.log('rendering, loading', loading);
    const cardStyles = loading ? `${styles.loading} ${styles.card}` : styles.card;
    const title = value.charAt(0).toUpperCase() + value.slice(1).toLowerCase();
    return(
      <div className={cardStyles}>
        {image && <img className={`${styles.image}`} src={image} alt={code} />}
        <div>{title} of {suit.toLowerCase()}</div>
      </div>
    );
  }
};

export default Card;
