const getDeckId = () => {
  console.log('api:getDeckId');
  return fetch('https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=6')
  .then(response => {
    if(response.ok) {
      return response.json();
    }
  })
  .then(data => data.deck_id);
}

const drawCard = (deckId, count = 1) => {
  console.log('api:drawCard', deckId);
  return fetch(`https://deckofcardsapi.com/api/deck/${deckId}/draw/?count=${count}`)
    .then(response => {
      if(response.ok) {
        return response.json();
      }
    })
    .then(data => data.cards);
};

export {
  getDeckId,
  drawCard,
}
