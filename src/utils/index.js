export const convertToScore = value => {
  if (isNaN(parseInt(value, 10))) {
    return value === 'ACE' ? 11 : 10;
  } else {
    return parseInt(value, 10);
  }
}
