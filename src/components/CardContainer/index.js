import React from 'react';

import Card from '../Card';

import styles from './styles.scss';

export default ({ cards }) => (
  <div className={`${styles['cards-container']}`}>
    {cards && cards.map((card, index) => (
      <Card key={index} {...card} />
    ))}
  </div>
)
