import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as actions from '../../actions';
import * as utils from '../../utils';

import Board from '../../components/Board/';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userTotal: 0,
      userBust: false,
      dealerTotal: 0,
      dealerBust: false,
      dealersTurn: false,
      winner: null,
    }

    this.calculateScore = this.calculateScore.bind(this);
    this.setupGame = this.setupGame.bind(this);
    this.drawCard = this.drawCard.bind(this);
    this.playAsDealer = this.playAsDealer.bind(this);
    this.passToDealer = this.passToDealer.bind(this);
  }
  componentWillMount() {
    this.setupGame();
  }
  componentWillReceiveProps(nextProps) {
    this.updateScores(nextProps);
  }
  calculateScore(cards) {
    let total = 0;
    let aces = 0;
    cards.forEach(card => {
      if (card.value === "ACE") {
        aces++;
      }
      total += utils.convertToScore(card.value)
    });

    while( aces > 0 && total > 21 ) {
      total -= 10;
      aces -= 1;
    }

    const bust = total > 21;
    return {
      bust,
      value: total,
    }
  }
  drawCard(dealer = false) {
    console.log('drawing for dealer?', dealer);
    dealer ? this.props.dealerLoadCard() : this.props.userLoadCard();
  }
  playAsDealer() {
    const {
      userTotal,
      dealerTotal
    } = this.state;
    if(userTotal < dealerTotal) {
      this.setState(state => ({
        winner: 'Dealer',
      }));
    } else {
      setTimeout(() => this.drawCard(true), 2000);
    }
  }
  passToDealer() {
    this.setState(state => ({
      dealersTurn: true,
    }), () => this.drawCard(true));
  }
  setupGame() {
    this.props.setupGame();
  }
  updateScores(props) {
    const { userCards, dealerCards, loading } = props;
    const { dealersTurn } = this.state;
    const dealerTotal = this.calculateScore(dealerCards);
    const userTotal = this.calculateScore(userCards);
    this.setState(state => ({
      dealerTotal: dealerTotal.value,
      dealerBust: dealerTotal.bust,
      userTotal: userTotal.value,
      userBust: userTotal.bust,
    }),
    () => {
      if (userTotal.bust) {
        this.setState(state => ({
          winner: 'Dealer',
        }));
      } else if (dealersTurn && !loading) {
        if (dealerTotal.bust) {
          this.setState(state => ({
            winner: 'User',
          }));
        } else {
          this.playAsDealer();
        }
      }
    });
  }
  render() {
    const {
      userCards,
      dealerCards,
    } = this.props;
    return (
      <Board
        onDraw={this.drawCard}
        onPass={this.passToDealer}
        dealerCards={dealerCards}
        userCards={userCards}
        {...this.state}
      />
    );
  }
}

const mapStateToProps = state => {
  return {...state};
}

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(App);
