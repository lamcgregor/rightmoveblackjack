import { call, put, all, takeEvery, select } from 'redux-saga/effects';
import * as actions from '../actions';
import * as api from '../api';

function* getDeckId() {
  const id = yield call(api.getDeckId);
  yield put(actions.deckLoaded(id));
  return id;
}

const selectDeckId = state => state.deckId;

function* setupGame() {
  yield call(getDeckId);
  yield call(drawCard, true);
  yield call(drawCard);
  yield call(drawCard);
}

function* drawCard(dealer = false) {
  const deckId = yield select(selectDeckId);
  const card = yield call(api.drawCard, deckId);
  dealer ? yield put(actions.dealerCardLoaded(card)) : yield put(actions.userCardLoaded(card));
}

function* watchUserDrawCard() {
  yield takeEvery(actions.USER_LOAD_CARD, drawCard, false);
}

function* watchDealerDrawCard() {
  yield takeEvery(actions.DEALER_LOAD_CARD, drawCard, true);
}

export function* watchSetupGame() {
  yield takeEvery(actions.LOAD_DECK, setupGame);
}

export default function* root() {
  yield all([
    watchUserDrawCard(),
    watchDealerDrawCard(),
    watchSetupGame(),
  ])
}
